package sda.servlety.service;

import sda.servlety.model.User;
import sda.servlety.repository.UserDAO;

import java.util.List;
import java.util.Optional;

public class UserService {

    private final UserDAO userDAO;

    public UserService() {
        this.userDAO = UserDAO.getInstance();
    }

    public Optional<User> createUser(String login, String password) {
        return userDAO.addUser(new User(login, password));
    }

    public Optional<User> getUser(int id) {
        return userDAO.getUserById(id);
    }

    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    public Optional<User> getUserByLogin(String login){
        return userDAO.getUserByLogin(login);
    }
}
