package sda.servlety.model;

import java.time.Instant;
import java.util.Objects;

public class Post {

    private Instant date;
    private  String text;
    private User user;

    public Post(String text, User user) {
        this.date = Instant.now();
        this.text = text;
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return date.equals(post.date) &&
                text.equals(post.text) &&
                user.equals(post.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, text, user);
    }

    @Override
    public String toString() {
        return "Post{" +
                "date=" + date +
                ", text='" + text + '\'' +
                ", user=" + user +
                '}';
    }
}

