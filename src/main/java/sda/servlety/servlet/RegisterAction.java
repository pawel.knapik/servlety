package sda.servlety.servlet;

import sda.servlety.model.User;
import sda.servlety.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet (urlPatterns = "/registerAction")
public class RegisterAction extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        this.userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        // walidacja parametrow
        Optional<User> userOptional = userService.createUser(login, password);

        if(userOptional.isPresent()){
            resp.sendRedirect("index.jsp");
        } else {
            System.out.println("User jest w bazie");
        }

        //przekierowanie na inna strone
    }
}
