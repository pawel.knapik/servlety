package sda.servlety.servlet;

import sda.servlety.model.User;
import sda.servlety.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/LoginAction")
public class LoginAction extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        // walidacja parametrow
        Optional<User> userByLogin = userService.getUserByLogin(login);

        //logika logowania
        if(userByLogin.isPresent() && passwordCheck(userByLogin.get(),password) ){

            User user = userByLogin.get();
            req.getSession().setAttribute("loggedUserId",user.getId());

            Cookie cookie = new Cookie("userCookieId",Integer.toString(user.getId()));
            resp.addCookie(cookie);
            resp.sendRedirect("secret/posts.jsp");

        }else {
            resp.sendRedirect("loginErrorPage.jsp");
        }

    }

    private boolean passwordCheck(User user, String requestPassword){
        return user.getPassword().equals(requestPassword);
    }
}
