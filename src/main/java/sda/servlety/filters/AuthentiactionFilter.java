package sda.servlety.filters;

import com.sun.deploy.net.HttpRequest;
import sda.servlety.service.UserService;

import javax.naming.AuthenticationException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter (urlPatterns = "/secret/*")
public class AuthentiactionFilter implements Filter {

    private UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        Cookie[] cookies = httpServletRequest.getCookies();

        //sprawdzenie czy jest cookie - bilte do wejscia
        String cookieValue = "";
        for (int i = 0; i <cookies.length ; i++) {
            if(cookies[i].getName().equals("userCookieId")){
                cookieValue = cookies[i].getValue();
            }
        }

        //sprawdzenie czy w sesji jest taki user
        Integer loggedUserId = (Integer) httpServletRequest.getSession().getAttribute("loggedUserId");

        if(cookieValue.equals("") || !cookieValue.equals(loggedUserId.toString())){
            //rzuc wyjatek
            throw new AuthException();
        }

        chain.doFilter(request,response);

    }

    @Override
    public void destroy() {

    }
}
