<%--
  Created by IntelliJ IDEA.
  User: knapikp
  Date: 3/24/2019
  Time: 10:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LoginError</title>
    <style>
        <%@include file="../../css/login.css"%>
    </style>
</head>
<body>

<%@include file="../pageHeader.jsp"%>

<%
    response.setStatus(401);
%>

<div id="loginErrorDiv">
    Niepoprawny login lub haslo
    <a href="login.jsp">Try again</a>
</div>

<%@include file="../pageFooter.jsp"%>

</body>
</html>
