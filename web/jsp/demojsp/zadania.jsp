<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: knapikp
  Date: 3/30/2019
  Time: 11:59 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Zadania</title>
    <style>
       <%@include file="../../css/zadania.css"%>
    </style>
</head>
<body>

<div id="header-div">

    <%
        String liczba1 = request.getParameter("liczba1");
        String liczba2 = request.getParameter("liczba2");

        if (liczba1 != null || liczba2 != null) {

            int wynikDodawania = Integer.parseInt(liczba1) + Integer.parseInt(liczba2);
            out.print("Wynik dodawania: " + wynikDodawania);
        } else {
            out.print("Podaj poprawne parametry");
        }
    %>

</div>

    <%
        //wyciagniecie atrybutow z reuestu
        String productName = request.getParameter("productName");
        String productCount = request.getParameter("productCount");
        if (productName == null || productCount == null) {
            out.print("Podaj parametry");
        } else {

            int productCountInt = Integer.parseInt(productCount);

            // wyziagniecie mapy produktow z sesji
            Map<String, Integer> productsMap = (Map<String, Integer>) session.getAttribute("productsMap");

            //gdy w sesji nie ma jeszcze mapy  produktami
            if (productsMap == null) {
                HashMap<String, Integer> newProductsMap = new HashMap<>();
                newProductsMap.put(productName, productCountInt);
                session.setAttribute("productsMap", newProductsMap);

            } else {

                Integer max =0;
                for (Map.Entry<String, Integer> mapEntity : productsMap.entrySet()) {
                    if(mapEntity.getValue()>max){
                        max = mapEntity.getValue();
                    }
                }
                response.addCookie(new Cookie("maxCountProduct",max.toString()));

                productsMap.put(productName, productCountInt);
                session.setAttribute("productsMap", productsMap);
            }
        }
    %>

    <%
        Map<String, Integer> producMaptoDisplay = (Map<String, Integer>) session.getAttribute("productsMap");
//        if (producMaptoDisplay == null) {                                                                     TESTOWO
//            out.print("Lista produktow jest pusta");
//        } else {
//            for (Map.Entry<String, Integer> mapEntity : producMaptoDisplay.entrySet()) {
//                out.print(mapEntity.getKey());
//            }
//        }
    %>

    <table style="border: 1px black solid">
        <tbody style="border: 1px black solid">
            <%
                if(producMaptoDisplay !=null){
                    for (Map.Entry<String, Integer> mapEntity : producMaptoDisplay.entrySet()) {
                        out.print(mapEntity.getKey());
            %>
                <tr>
                    <td>
                        <% out.print(mapEntity.getKey()); %>
                    </td>
                    <td>
                        <% out.print(mapEntity.getValue()); %>
                    </td>
                </tr>
        <%}%>
        <%}%>

        </tbody>
    </table>

    <div>
        Pamietaj o ${cookie.get("maxCountProduct")}
    </div>

    <jsp:include page="exercisesDemo.jsp"/>

                                                    <%--przekierowanie --%>
                                                     <%--<jsp:forward page="login.jsp"/>--%>

    <h1>
        Using Beans
    </h1>

    <jsp:useBean id="calculatorBean" class="sda.servlety.demoJsp.MyCalculatorBean" scope="request"/>
    <jsp:setProperty name="calculatorBean" property="addingNumber" value="2"/>
    <jsp:setProperty name="calculatorBean" property="multiplyNumber" value="10"/>

    <%
        int add = calculatorBean.add(100);
        int multiply = calculatorBean.multiply(100);

        out.print(add);
        out.print(multiply);

    %>

</body>
</html>
