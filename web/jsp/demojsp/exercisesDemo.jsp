<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: knapikp
  Date: 3/30/2019
  Time: 9:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<div>

    <h1>
        Expresion w JSP
    </h1>

    <p>
        <%= java.time.LocalDateTime.now()%>
    </p>

    <p>
        <%=10 + 200%>
    </p>

    <h1>
        Skyptlet
    </h1>

    <p>
            <% for (int i = 0; i < 10; i++) {%>
    <h1><% out.print(i); %></h1>
    <%}%>

    </p>

    <table style="border: 1px solid black">
        <thead>
            <tr>
                <td>Liczba</td>
                <td>Potega</td>
            </tr>
        </thead>

        <tbody>
        <% for (int i = 1; i <6 ; i++) {
            out.print("<tr><td>" + i + "</td><td>" + i * i + "</td> </tr>");
        }
        %>

        <% for (int i = 1; i <6 ; i++) {%>
        <tr>
            <td> <%= i %></td>
            <td> <%= i * i %></td>
        </tr>

        <%}%>

        </tbody>
    </table>

    <h1>
        Dyrektywy
    </h1>

    <%! private int visitCount = 0; %>
    Liczba wyswietlen: <%=visitCount%>
    <%
        visitCount++;
    %>

</div>


</body>
</html>


